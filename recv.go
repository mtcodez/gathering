package main

import (
	"fmt"
	"log"
	// "time"

	"github.com/streadway/amqp"
	// "stats"
	"bufio"
	"flag"
	"os"
	// "strings"
)

const (
	fileName = "./stats.json"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func main() {
	amqpURL := flag.String("amqp", "amqp://mtcodez:mtcodeztest@192.168.1.130:5672/", "Specify ampq URL.")
	nodes := flag.String("nodes", "test", "Specify receiving nodes.")
	info := flag.String("info", "", "Specify the brief of the experiment.")
	flag.Parse()
	// nodeArray := strings.Split(*nodes, ",")
	// hangon := make(chan bool)
	conn, err := amqp.Dial(*amqpURL)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	// for _, node := range nodeArray {
	// go func(node string, info string, amqpURL string) {

	// t := time.Now()

	// f, err := os.Create(*info + "_[" + node + "]" + t.Format(time.RFC3339) + ".json")
	f, err := os.Create(*info + "_" + *nodes + "" + ".json")
	failOnError(err, "Failed when creating file.")
	defer f.Close()
	w := bufio.NewWriter(f)

	q, err := ch.QueueDeclare(
		*nodes, // name
		false,  // durable
		false,  // delete when usused
		false,  // exclusive
		false,  // no-wait
		nil,    // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			_, err := w.WriteString(string(d.Body[:]) + "\n")
			failOnError(err, "Failed when writing json to file")
			log.Printf("Received a message: %s", d.Body)
			w.Flush()
		}
	}()
	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
	// }()
	// }
	// reader := bufio.NewReader(os.Stdin)
	// input, _ := reader.ReadString('\n')
	// fmt.Println(input)
	// <-hangon

}
