# Gathering
This module aims at providing a metric-gathering program written in golang. There are two parts, with the first one polling from procfs and the second one using RESTful requests.

MongoDB is chosen to store all the data and exports to csv when needed.

REMEMBER to change the RabbitMQ configuration, and comment out the binding ip( to allow every address instead of just localhost)