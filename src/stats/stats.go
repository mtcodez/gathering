package stats

import (
	"encoding/json"
	"fmt"
	linuxproc "github.com/c9s/goprocinfo/linux"
	"log"
	"net/http"
	"net/url"
	"os/exec"
	"strings"
	// "os"
	"strconv"
	"time"
)

type Gatherer struct {
	JitsiURL string
	client   *http.Client
}

func NewGatherer(url string) *Gatherer {
	tr := &http.Transport{ResponseHeaderTimeout: time.Duration(3 * time.Second)}
	client := &http.Client{
		Transport: tr,
		Timeout:   time.Duration(4 * time.Second),
	}
	return &Gatherer{url, client}
}

type Stats struct {
	Sys   SysStats
	Jitsi JitsiStats
}

type StatsWithoutJitsi struct {
	Sys SysStats
}

type SysStats struct {
	// CPU
	CPUUser         uint64  `json:"cpu_user"`
	CPUSystem       uint64  `json:"cpu_system"`
	CPUUserP        float64 `json:"cpu_user_p"`
	CPUSysP         float64 `json:"cpu_sys_p"`
	CPUIOWaitP      float64 `json:"cpu_iowai_p"`
	CPUIOWait       uint64  `json:"user_iowait"`
	Interrupts      uint64  `json:"intr"`
	ContextSwitches uint64  `json:"ctxt"`
	// loadavg
	LoadAvg1  float64 `json:"load_avg_1"`
	LoadAvg5  float64 `json:"load_avg_5"`
	LoadAvg15 float64 `json:"load_avg_15"`
	// Diskstats
	DiskReadIOs      uint64  `json:"disk_read_ios"`
	DiskWriteIOs     uint64  `json:"disk_write_ios"`
	DiskReadSectors  uint64  `json:"disk_read_sectors"`
	DiskWriteSectors uint64  `json:"disk_write_sectors"`
	DiskIOTicks      uint64  `json:"disk_io_ticks"`
	IOPS             float64 `json:"iops"`
	DiskReadPerSec   float64 `json:"disk_read_per_sec"`
	DiskWritePerSec  float64 `json:"disk_write_per_sec"`
	// NetworkStat
	RxBytes   uint64 `json:"rx_bytes"`
	RxPackets uint64 `json:"rx_packets"`
	TxBytes   uint64 `json:"tx_bytes"`
	TxPackets uint64 `json:"tx_packets"`
}

type JitsiStatsStr struct {
	CPUUsage        float64 `json:"cpu_usage,string"`
	UsedMemory      int64   `json:"used_memory"`
	RTPLoss         float64 `json:"rtp_loss,string"`
	BitRateDownload float64 `json:"bit_rate_download,string"`
	AudioChannels   int64   `json:"audiochannels"`
	BitRateUpload   float64 `json:"bit_rate_upload,string"`
	Conferences     int64   `json:"conferences"`
	Participants    int64   `json:"participants"`
	CurrentTimestamp  string  `json:"current_timestamp"`
	// GracefulShutdown bool    `json:"graceful_shutdown"`
	Threads       int64 `json:"threads"`
	TotalMemory   int64 `json:"total_memory"`
	VideoChannels int64 `json:"videochannels"`
	VideoStreams  int64 `json:"videostreams"`
	AvgUploadJitter   float64 `json:"avg_upload_jitter"`
	AvgDownloadJitter float64 `json:"avg_download_jitter"`
}
type JitsiStats struct {
	CPUUsage        float64 `json:"cpu_usage"`
	UsedMemory      int64   `json:"used_memory"`
	RTPLoss         float64 `json:"rtp_loss"`
	BitRateDownload float64 `json:"bit_rate_download"`
	AudioChannels   int64   `json:"audiochannels"`
	BitRateUpload   float64 `json:"bit_rate_upload"`
	Conferences     int64   `json:"conferences"`
	Participants    int64   `json:"participants"`
	CurrentTimestamp  string  `json:"current_timestamp"`
	// GracefulShutdown bool    `json:"graceful_shutdown"`
	Threads           int64   `json:"threads"`
	TotalMemory       int64   `json:"total_memory"`
	VideoChannels     int64   `json:"videochannels"`
	VideoStreams      int64   `json:"videostreams"`
	AvgUploadJitter   float64 `json:"avg_upload_jitter"`
	AvgDownloadJitter float64 `json:"avg_download_jitter"`
}

func (g *Gatherer) Gather() *Stats {
	sys := g.gatherSysStats()
	jitsi, err := g.gatherJitsiStats()
	failOnError(err, "Jitsi Gatherer failed.")
	return &Stats{*sys, *jitsi}
}

func (g *Gatherer) GatherWithoutJitsi() *StatsWithoutJitsi {
	sys := g.gatherSysStats()
	return &StatsWithoutJitsi{*sys}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func (g *Gatherer) gatherJitsiStats() (*JitsiStats, error) {
	_, err := url.Parse(g.JitsiURL + ":8080/colibri/stats")
	if err != nil {
		return nil, fmt.Errorf("unable to parse given server url %s: %s", g.JitsiURL, err)
	}

	resp, err := g.client.Get(g.JitsiURL + ":8080/colibri/stats")
	if err != nil {
		return nil, fmt.Errorf("unable to decode jitsi response: %s", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("jitsi responded with unexepcted status code %d", resp.StatusCode)
	}
	stats := &JitsiStatsStr{}
	if err := json.NewDecoder(resp.Body).Decode(stats); err != nil {
		return nil, fmt.Errorf("unable to decode jitsi response: %s", err)
	}
	out := &JitsiStats{}
	out.CPUUsage = stats.CPUUsage
	out.UsedMemory = stats.UsedMemory
	out.RTPLoss = stats.RTPLoss
	out.BitRateDownload = stats.BitRateDownload
	out.AudioChannels = stats.AudioChannels
	out.BitRateUpload = stats.BitRateUpload
	out.Conferences = stats.Conferences
	out.Participants = stats.Participants
	out.CurrentTimestamp = stats.CurrentTimestamp
	// out.GracefulShutdown = stats.GracefulShutdown
	out.Threads = stats.Threads
	out.TotalMemory = stats.TotalMemory
	out.VideoChannels = stats.VideoChannels
	out.VideoStreams = stats.VideoStreams
	out.AvgUploadJitter = stats.AvgUploadJitter
	out.AvgDownloadJitter = stats.AvgDownloadJitter
	return out, nil
}

func (g *Gatherer) gatherSysStats() *SysStats {
	stat, err := linuxproc.ReadStat("/proc/stat")
	failOnError(err, "Failed when reading /proc/stat.")

	sysStats := &SysStats{}
	// CPU
	sysStats.CPUUser = stat.CPUStatAll.User
	sysStats.CPUSystem = stat.CPUStatAll.System
	sysStats.CPUIOWait = stat.CPUStatAll.IOWait
	sysStats.Interrupts = stat.Interrupts
	sysStats.ContextSwitches = stat.ContextSwitches
	// loadavg
	loadavg, err := linuxproc.ReadLoadAvg("/proc/loadavg")
	failOnError(err, "Failed when reading /proc/loadavg.")

	sysStats.LoadAvg1 = loadavg.Last1Min
	sysStats.LoadAvg5 = loadavg.Last5Min
	sysStats.LoadAvg15 = loadavg.Last15Min
	// Diskstats
	diskStats, err := linuxproc.ReadDiskStats("/proc/diskstats")
	failOnError(err, "Failed when reading /proc/diskstats.")
	for _, s := range diskStats {
		sysStats.DiskReadIOs += s.ReadIOs
		sysStats.DiskWriteIOs += s.WriteIOs
		sysStats.DiskReadSectors += s.ReadSectors
		sysStats.DiskWriteSectors += s.WriteSectors
		sysStats.DiskIOTicks += s.IOTicks
	}
	// NetworkStats
	networkStat, err := linuxproc.ReadNetworkStat("/proc/net/dev")
	for _, s := range networkStat {
		sysStats.RxBytes += s.RxBytes
		sysStats.RxPackets += s.RxPackets
		sysStats.TxBytes += s.TxBytes
		sysStats.TxPackets += s.TxPackets
	}

	// using iostat
	// %user, %system, %iowait
	cpuOut, err := exec.Command("sh", "-c", "iostat | awk '/^ /{print $1,$3,$4}'").Output()
	failOnError(err, "iostat failed cpu.")
	diskOut, err := exec.Command("sh", "-c", "iostat | awk '/sda/ { print $2,$3,$4}'").Output()
	failOnError(err, "iostat failed on disk.")
	cpuSplit := strings.Fields(string(cpuOut))
	diskSplit := strings.Fields(string(diskOut))
	fcup, err := strconv.ParseFloat(cpuSplit[0], 64)
	sysStats.CPUUserP = fcup
	fcsp, err := strconv.ParseFloat(cpuSplit[1], 64)
	sysStats.CPUSysP = fcsp
	fcip, err := strconv.ParseFloat(cpuSplit[2], 64)
	sysStats.CPUIOWaitP = fcip
	fiops, err := strconv.ParseFloat(diskSplit[0], 64)
	sysStats.IOPS = fiops
	fdrps, err := strconv.ParseFloat(diskSplit[1], 64)
	sysStats.DiskReadPerSec = fdrps
	fdwps, err := strconv.ParseFloat(diskSplit[2], 64)
	sysStats.DiskWritePerSec = fdwps
	return sysStats
}
