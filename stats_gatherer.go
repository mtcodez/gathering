package stats

import (
	"encoding/json"
	"fmt"
	linuxproc "github.com/c9s/goprocinfo/linux"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

type Gatherer struct {
	JitsiURL string
	client   *http.Client
}

func NewGatherer(url string) *Gatherer {
	tr := &http.Transport{ResponseHeaderTimeout: time.Duration(3 * time.Second)}
	client := &http.Client{
		Transport: tr,
		Timeout:   time.Duration(4 * time.Second),
	}
	return &Gatherer{url, client}
}

type Stats struct {
	Sys   SysStats
	Jitsi JitsiStats
}

type SysStats struct {
	// CPU
	CPUUser         uint64 `json:"cpu_user"`
	CPUSystem       uint64 `json:"cpu_system"`
	CPUIOWait       uint64 `json:"user_iowait"`
	Interrupts      uint64 `json:"intr"`
	ContextSwitches uint64 `json:"ctxt"`
	// loadavg
	LoadAvg1  float64 `json:"load_avg_1"`
	LoadAvg5  float64 `json:"load_avg_5"`
	LoadAvg15 float64 `json:"load_avg_15"`
	// Diskstats
	DiskReadIOs      uint64 `json:"disk_read_ios"`
	DiskWriteIOs     uint64 `json:"disk_write_ios"`
	DiskReadSectors  uint64 `json:"disk_read_sectors"`
	DiskWriteSectors uint64 `json:"disk_write_sectors"`
	DiskIOTicks      uint64 `json:"disk_io_ticks"`
	// NetworkStat
	RxBytes   uint64 `json:"rx_bytes"`
	RxPackets uint64 `json:"rx_packets"`
	TxBytes   uint64 `json:"tx_bytes"`
	TxPackets uint64 `json:"tx_packets"`
}

type JitsiStats struct {
	CPUUsage         string `json:"cpu_usage"`
	UsedMemory       int64  `json:"used_memory"`
	RTPLoss          string `json:"rtp_loss"`
	BitRateDownload  string `json:"bit_rate_download"`
	AudioChannels    int64  `json:"audiochannels"`
	BitRateUpload    string `json:"bit_rate_upload"`
	Conferences      int64  `json:"conferences"`
	Participants     int64  `json:"participants"`
	CurrentTimestap  string `json:"current_timestap"`
	GracefulShutdown bool   `json:"graceful_shutdown"`
	Threads          int64  `json:"threads"`
	TotalMemory      int64  `json:"total_memory"`
	VideoChannels    int64  `json:"videochannels"`
	VideoStreams     int64  `json:"videostreams"`
}

func main() {
	// every 1 sec

	if len(os.Args) != 2 {
		fmt.Println("At least one parameter for the URL of Jitsi.")
		return
	}
	ticker := time.NewTicker(time.Millisecond * 3000)
	gatherer := NewGatherer(os.Args[1])
	forever := make(chan bool)
	go func() {
		for t := range ticker.C {
			stats := gatherer.Gather()
			fmt.Println("Tick at", t)
			// if err := json.NewEncoder(w).Encode(stats); err != nil {
			// 	log.Fatal("json encoding error %s", err)
			// }
			jsonStr, _ := json.Marshal(stats)
			fmt.Println(string(jsonStr))
		}
	}()
	<-forever
	defer ticker.Stop()
	fmt.Println("Ticker stopped")
}

func (g *Gatherer) Gather() *Stats {
	sys := g.gatherSysStats()
	jitsi, err := g.gatherJitsiStats()
	failOnError(err, "Jitsi Gatherer failed.")
	return &Stats{*sys, *jitsi}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func (g *Gatherer) gatherJitsiStats() (*JitsiStats, error) {
	_, err := url.Parse(g.JitsiURL + ":8080/colibri/stats")
	if err != nil {
		return nil, fmt.Errorf("unable to parse given server url %s: %s", g.JitsiURL, err)
	}

	resp, err := g.client.Get(g.JitsiURL + ":8080/colibri/stats")
	if err != nil {
		return nil, fmt.Errorf("unable to decode jitsi response: %s", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("jitsi responded with unexepcted status code %d", resp.StatusCode)
	}
	stats := &JitsiStats{}
	if err := json.NewDecoder(resp.Body).Decode(stats); err != nil {
		return nil, fmt.Errorf("unable to decode jitsi response: %s", err)
	}
	return stats, nil
}

func (g *Gatherer) gatherSysStats() *SysStats {
	stat, err := linuxproc.ReadStat("/proc/stat")
	failOnError(err, "Failed when reading /proc/stat.")

	sysStats := &SysStats{}
	// CPU
	sysStats.CPUUser = stat.CPUStatAll.User
	sysStats.CPUSystem = stat.CPUStatAll.System
	sysStats.CPUIOWait = stat.CPUStatAll.IOWait
	sysStats.Interrupts = stat.Interrupts
	sysStats.ContextSwitches = stat.ContextSwitches
	// loadavg
	loadavg, err := linuxproc.ReadLoadAvg("/proc/loadavg")
	failOnError(err, "Failed when reading /proc/loadavg.")

	sysStats.LoadAvg1 = loadavg.Last1Min
	sysStats.LoadAvg5 = loadavg.Last5Min
	sysStats.LoadAvg15 = loadavg.Last15Min
	// Diskstats
	diskStats, err := linuxproc.ReadDiskStats("/proc/diskstats")
	failOnError(err, "Failed when reading /proc/diskstats.")
	for _, s := range diskStats {
		sysStats.DiskReadIOs += s.ReadIOs
		sysStats.DiskWriteIOs += s.WriteIOs
		sysStats.DiskReadSectors += s.ReadSectors
		sysStats.DiskWriteSectors += s.WriteSectors
		sysStats.DiskIOTicks += s.IOTicks
	}
	// NetworkStats
	networkStat, err := linuxproc.ReadNetworkStat("/proc/net/dev")
	for _, s := range networkStat {
		sysStats.RxBytes += s.RxBytes
		sysStats.RxPackets += s.RxPackets
		sysStats.TxBytes += s.TxBytes
		sysStats.TxPackets += s.TxPackets
	}
	return sysStats
}
