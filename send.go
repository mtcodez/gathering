package main

import (
	"fmt"
	"log"

	"encoding/json"
	"flag"
	"github.com/streadway/amqp"
	// "os"
	"stats"
	"time"
)

const (
// amqpUrl  = "amqp://mtcodez:mtcodeztest@192.168.1.130:5672/"
// interval = 0.5 * 1000
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func main() {
	// enableSys := flag.Bool("sys", true, "Specify if output the system statistics.")
	// enableJitsi := flag.Bool("jitsi", true, "Specify if output the jitsi statistics.")
	var interval int
	jitsiURL := flag.String("jitsiurl", "", "Specify the Jitsi stats URL.")
	amqpURL := flag.String("amqp", "amqp://mtcodez:mtcodeztest@192.168.1.130:5672/", "Specify the ampq url.")
	flag.IntVar(&interval, "interval", 1000, "Specify the gathering interval.")
	queueName := flag.String("queue", "test", "Specify the queue name in RabbitMQ.")
	flag.Parse()
	fmt.Println("interval", interval)

	conn, err := amqp.Dial(*amqpURL)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		*queueName, // name
		false,      // durable
		false,      // delete when unused
		false,      // exclusive
		false,      // no-wait
		nil,        // arguments
	)
	failOnError(err, "Failed to declare a queue")

	ticker := time.NewTicker(time.Millisecond * time.Duration(interval))
	gatherer := stats.NewGatherer(*jitsiURL)
	forever := make(chan bool)
	go func() {
		for t := range ticker.C {
			var jsonStr []byte
			if *jitsiURL == "" {
				statsData := gatherer.GatherWithoutJitsi()
				jsonStr, _ = json.Marshal(statsData)
			} else {
				statsData := gatherer.Gather()
				jsonStr, _ = json.Marshal(statsData)
			}
			fmt.Println("Tick at", t)
			// if err := json.NewEncoder(w).Encode(stats); err != nil {
			// 	log.Fatal("json encoding error %s", err)
			// }

			fmt.Println(string(jsonStr))
			body := string(jsonStr)
			err = ch.Publish(
				"",     // exchange
				q.Name, // routing key
				false,  // mandatory
				false,  // immediate
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        []byte(body),
				})
			failOnError(err, "Failed to publish a message")
		}
	}()
	<-forever
	defer ticker.Stop()
	fmt.Println("Ticker stopped")
}
